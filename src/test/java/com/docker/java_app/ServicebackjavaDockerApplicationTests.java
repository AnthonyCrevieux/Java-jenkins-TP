package com.docker.java_app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.docker.java_app.model.Formulaire;

@SpringBootTest
class ServicebackjavaDockerApplicationTests {

	@Test
	void contextLoads() {
	}
	
	public String postHello(String name) {
		Formulaire form = new Formulaire();
		form.setName(name);
		form.setResponse("Bonjour " + form.getName().toString() + ", vous êtes bien matinal !");
		String response = form.getResponse().toString();
		
		return response;
	
	}
	
	@Test
	public void whenEnterName_thenReturnString () {
		
		assertEquals("Bonjour Anthony, vous êtes bien matinal !",postHello("Anthony"));
		
	}

}
